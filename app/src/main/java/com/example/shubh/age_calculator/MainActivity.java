package com.example.shubh.age_calculator;

import android.os.CountDownTimer;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    TextInputEditText t_month,t_day,t_year;
    String month,day,year;
    int imonth,iday,iyear,cmonth,cday,cyear,maxday,a_day;
    int age_day,age_month,age_year,day_pass;
    Button btn;
    TextView tv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t_day = findViewById(R.id.day);
        t_month = findViewById(R.id.month);
        t_year = findViewById(R.id.year);
        btn = findViewById(R.id.button);
        tv1 = findViewById(R.id.textView2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();

                //----------**********Getting current date day/month/year.**********----------
                cday = cal.get(Calendar.DAY_OF_MONTH);
                cmonth = cal.get(Calendar.MONTH);
                cmonth = cmonth+1;
                cyear = cal.get(Calendar.YEAR);

                //----------**********Getting input date day/month/year.**********----------
                day = t_day.getText().toString();
                month = t_month.getText().toString();
                year = t_year.getText().toString();

                //----------**********Validate input date day/month/year.**********----------
                if (day.equals("") || month.equals("") || year.equals("")) {
                    if (day.equals(""))
                        t_day.setError("Please Input day");
                    if (month.equals(""))
                        t_month.setError("Please Input month");
                    if (year.equals(""))
                        t_year.setError("Please Input year");
                    return;
                }
                else {
                    iday = Integer.parseInt(day);
                    imonth = Integer.parseInt(month);
                    iyear = Integer.parseInt(year);
                    //----------**********Validating MONTHS.**********----------
                    if(imonth < 1 || imonth > 12) {
                        t_month.setError("Please input a Valid Month");
                        return;
                    }
                    else{
                        if(imonth == 1 || imonth == 3 || imonth == 5 || imonth == 7 || imonth == 8 || imonth == 10 || imonth == 12)
                            maxday = 31;
                        else if (imonth == 2){
                            if (iyear%4==0)
                                maxday = 29;
                            else
                                maxday = 28;
                        }
                        else if (imonth == 4 || imonth == 6 || imonth == 9 || imonth == 11)
                            maxday = 30;

                    }

                    //----------**********Validating DAY.**********----------
                    if (iday < 1 || iday > maxday) {
                        t_day.setError("Please enter a valid DAY");
                        return;
                    }
                    else
                        a_day = maxday - iday;

                    //----------**********Validating YEAR.**********----------
                    if (iyear < (cyear-100) || iyear > cyear) {
                        t_year.setError("Please enter a valid YEAR");
                        return;
                    }
                    if(iyear == cyear){
                        if (imonth < cmonth){}
                        else if (imonth == cmonth){
                            if(iday < cday) {}
                            else if (iday >cday){
                                t_day.setError("Please Enter a valid day");
                                return;
                            }
                        }
                        else {
                            t_month.setError("Please Enter a valid month");
                            return;
                        }
                    }

                }


                //----------**********Calculating AGE**********----------
                //----------********** Calculating YEAR **********----------
                age_year = cyear - iyear;

                //----------********** Calculating DAY **********----------
                if(cday < iday) {
                    age_day = cday + a_day;
                    day_pass = 0;
                }
                else {
                    age_day = cday - iday;
                    day_pass = 1;
                }
                //----------********** Calculating MONTH **********----------
                if(cmonth > imonth) {
                    if(day_pass == 1) {
                        age_month = (cmonth - imonth);
                    }
                    else {
                        age_month = (cmonth - imonth) - 1;
                    }
                }
                else {
                    age_year--;
                    if(day_pass == 1) {
                        age_month = (cmonth + (12 - imonth));
                        if(age_month == 12){
                            age_month = 0;
                            age_year++;
                        }
                    }
                    else {
                        age_month = cmonth + (12 - imonth) - 1;
                    }
                }
                tv1.setText("You are "+age_day+" day "+age_month+" month "+age_year+" year old ");
            }
        });
    }
}
